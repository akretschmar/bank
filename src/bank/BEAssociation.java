package bank;

public class BEAssociation {

    public static void main(String[] args) {
      Bank bank = new Bank("Bank of Montreal");
      Employee emp = new Employee("Jean");
      
      System.out.println(emp.getEmployeeName() + " is an employee of "
                        + bank.getBankName());
    }
    
}
